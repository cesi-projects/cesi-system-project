#!/bin/bash

#################
# CONFIGURATION #
#################

# IP HTTP server
DOMAIN="carnofluxe.domain"
IP_HTTP="192.168.10.10"
DNS="ns1.carnofluxe.domain"
# Number of packets send
PACKET="2"
CSV_OUTPUT="/opt/scripts/supervision.csv"

SSH_USER="root"
SSH_HOST="192.168.10.10"
SSH_PATH="/opt/scripts/"

#################


date=`date '+%Y-%m-%d %H:%M:%S'`

# Test ping response

pingStatus="Error"

pingRequest=`ping -c $PACKET $IP_HTTP | grep -Eo '[0-9]{1,} received' | grep -Eo '[0-9]{1,}'`

if [ $pingRequest = $PACKET ]; then
	pingStatus="Ok"
fi


# Test DNS response

dnsStatus="Error"

# -o only the pattern
dnsRequest=`dig $DNS | grep status | grep -o 'NOERROR'`

if [ "$dnsRequest" = "NOERROR" ]; then
	dnsStatus="Ok"
fi


# Test HTTP response

# -s silent mode
# -o write to file instead of stdout
httpRequest=`curl -s -o /dev/null -w '%{http_code}\n%{time_total}' $DOMAIN`

httpRespCode=`echo $httpRequest | cut -d' ' -f1`
httpTime=`echo $httpRequest | cut -d' ' -f2 | tr , .`

httpStatus="Error"

if echo $httpRespCode | grep -Eo '[2]+[0-9]{2}'; then
	httpStatus="Ok"
fi

echo "$date, $pingStatus, $dnsStatus, $httpStatus, $httpRespCode, $httpTime" >> $CSV_OUTPUT

# Copy the CSV on the web server
scp $CSV_OUTPUT $SSH_USER@$SSH_HOST:$SSH_PATH