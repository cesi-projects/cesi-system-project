#!/bin/bash

#################
# CONFIGURATION #
#################

DIR_BACKUP="/media/backup_diff"
DIR_SAVED_CARNO="/var/www/carnofluxe.domain"
DIR_SAVED_WIKICARNO="/var/www/wiki.carnofluxe.domain"
DIR_SAVED_CONFIG="/etc/apache2/sites-available"
DATE=`date '+%d-%m-%Y à %Hh%Mmin%Ss'`

#################

rdiff-backup --remove-older-than 6M --force $DIR_BACKUP

ERROR=`rdiff-backup-statistics $DIR_BACKUP | grep "Errors 0"`

if [ "$ERROR" != "Errors 0" ] ; then
        printf "Subject:Erreur de sauvegarde\nUne erreur est survenue lors de la sauvegarde journalière de votre service ! Cette erreur est arrivé le $DATE " | msmtp adel.louarrani@viacesi.fr
fi

rdiff-backup --include $DIR_SAVED_CARNO --include $DIR_SAVED_WIKICARNO --include $DIR_SAVED_CONFIG $DIR_BACKUP