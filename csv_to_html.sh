#!/bin/bash

#################
# CONFIGURATION #
#################

CSV_HTTP_LOG="/opt/scripts/http_log.csv"
CSV_SUPERVISION="/opt/scripts/supervision.csv"

HTML_HTTP_LOG="/var/www/carnofluxe.domain/http_log.html"
HTML_SUPERVISION="/var/www/carnofluxe.domain/supervision.html"

#################

echo "<meta http-equiv=\"refresh\" content=\"60\">" > $HTML_HTTP_LOG
echo "<table>" >> $HTML_HTTP_LOG
echo "tr><th>Date</th><th>IP</th></tr>" >> $HTML_HTTP_LOG
cat $CSV_HTTP_LOG | while IFS= read INPUT; do
    echo "<tr><td>${INPUT//,/</td><td>}</td></tr>" >> $HTML_HTTP_LOG
done
echo "</table>" >> $HTML_HTTP_LOG


echo "<meta http-equiv=\"refresh\" content=\"60\">" > $HTML_SUPERVISION
echo "<table>" >> $HTML_SUPERVISION
echo "tr><th>Date</th><th>Ping</th><th>DNS Status</th><th>HTTP Status</th><th>HTTP Code</th><th>Temps de reponse</th></tr>" >> $HTML_SUPERVISION
tac $CSV_SUPERVISION | sed -n -e 1,25p | while IFS= read INPUT; do
    echo "<tr><td>${INPUT//,/</td><td>}</td></tr>" >> $HTML_SUPERVISION
done
echo "</table>" >> $HTML_SUPERVISION


#csv2html $CSV_HTTP_LOG -c > $HTML_HTTP_LOG
#csv2html $CSV_SUPERVISION -c > $HTML_SUPERVISION
