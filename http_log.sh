#!/bin/bash

#################
# CONFIGURATION #
#################

# apache2 log file
file="/var/log/apache2/access.log"
# File output CSV
fileCSV="/opt/scripts/http_log.csv"

#################


# now in timestamp
now=`date -d '1 hour ago' +%s`

echo "Récupération des adresses IP ..."

getMonth() {
	case $1 in
		"Jan") echo "01"
		;;
		"Feb") echo "02"
		;;
		"Mar") echo "03"
		;;
		"Apr") echo "04"
		;;
		"May") echo "05"
		;;
		"Jun") echo "06"
		;;
		"Jul") echo "07"
		;;
		"Aug") echo "08"
		;;
		"Sep") echo "09"
		;;
		"Oct") echo "10"
		;;
		"Nov") echo "11"
		;;
		"Dec") echo "12"
		;;
	esac
}

echo "Date, IP" > $fileCSV

tac $file | while IFS= read line; do

  	date=`echo $line | cut -d' ' -f4 | cut -d'[' -f2`
	
	DAY=`echo $date | cut -d'/' -f1`
	monthName=`echo $date | cut -d'/' -f2`
	MONTH=`getMonth $monthName`
	YEAR=`echo $date | cut -d'/' -f3 | cut -d':' -f1`
	TIME=`echo $date | cut -d':' -f2,3,4`

   	cond=`date -d "$YEAR-$MONTH-$DAY $TIME" +%s`

	if [ $cond -gt $now ]; then
		ip=`echo $line | cut -d' ' -f1`
		echo "$YEAR-$MONTH-$DAY $TIME, $ip" >> $fileCSV
	else
		break
	fi
	
done