# CESI Projet Système - Groupe 5

réalisé par Adel, Matthieu, Antoine et Adrien

# Installation du Serveur Web

Installer le paquet **apache2**:
``` bash
apt-get install apache2
```
Créer un repertoire pour les fichiers html:
``` bash
mkdir -p /var/www/carnofluxe.domain
```
Modification des autorisations:  
``` bash
chown -R $USER:$USER /var/www/carnofluxe.domain 
chmod -R 755 /var/www/carnofluxe.domain
```
Nous placerons les fichiers html dans ce répertoire.

Création du fichier **carnofluxe.domain.conf** dans le répertoire **sites-available** de apache2:
``` bash
nano /etc/apache2/sites-available/carnofluxe.domain.conf
```

``` bash
<VirtualHost *:80>

	ServerName carnofluxe.domain
	DocumentRoot /var/www/carnofluxe.domain
		
	<Directory /var/www/carnofluxe.domain>
                Order Deny,Allow
                Deny from all
                Allow from localhost
                Allow from 192.168
                Allow from 10
        </Directory>

	
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```
(Voir fichier: [carnofluxe.domain.conf](https://gitlab.com/cesi-projects/cesi-system-project/blob/master/carnofluxe.com.conf))

Pour activer le site site on utilise la commande:
``` bash
a2ensite carnofluxe.domain.conf
```
Ajouter dans le fichier /etc/hosts la ligne:  
``` bash
127.0.1.1  carnofluxe.domain   
```
Ajouter dans le fichier /etc/apache2/apache2.conf la ligne:
``` bash
ServerName carnofluxe.domain
```

Puis on reload apache2
``` bash
systemctl reload apache2
```
La mise en place du serveur wiki fonctionne de la même manière.
On crée un VirtualHost
``` bash

<VirtualHost *:80>

        ServerName wiki.carnofluxe.domain
        DocumentRoot /var/www/wiki.carnofluxe.domain

        <Directory /var/www/wiki.carnofluxe.domain>
                Order Deny,Allow
                Deny from all
                Allow from localhost
                Allow from 192.168
                Allow from 10
                Satisfy Any
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```
(Voir fichier: [wiki.carnofluxe.domain](https://gitlab.com/cesi-projects/cesi-system-project/blob/master/wiki.carnofluxe.domain.conf))

On ajoute ensuite les fichiers de dokuwiki dans le répertoire /var/www/wiki.carnofluxe.domain

Puis on reload apache2
``` bash
systemctl reload apache2
```
## Installation du wiki
Installer le paquet libapache2-mod-php php-xml
``` bash
apt intall libapache2-mod-php php-xml
```
Autoriser la réécriture d'apache
``` bash
a2enmod rewrite
```
Redemarrer le service Apache
``` bash
systemctl restart apache2
```
Aller dans le repertoire cd/var/www/
``` bash
cd /var/www/
```
Telecharger l'archive contenant Dokuwiki
``` bash
wget https://download.dokuwiki.org/src/dokuwiki/dokuwiki-stable.tgz
```
Extraire l'archive
``` bash
tar xvf dokuwiki-stable.tgz
```
Déplacer le dossier dans le répertoire /var/www/wiki.carnoflux.domain
``` bash
mv dokuwiki-*/ wiki.carnofluxe.domain/
```
On change le propriétaire du dossier récursivement
``` bash
chown -R www-data:www-data /var/www/wiki.carnofluxe.domain/
```
Modifier apache2.conf
``` bash
sudo nano /etc/apache2/apache2.conf
```
En remplacant
``` bash
<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride None
        Require all granted
</Directory>
``` 
Par
``` bash
<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>
```
Relancer le service apache2
``` bash
sudo service apache2 restart
```
Lancer la configuration du site en lançant sur internet
``` bash
INSTALL 127.0.0.1/install.php 
```
Puis supprimer cette page
``` bash
rm /var/www/dokuwiki/install.php
```

## Installation du serveur SSH
Installation du paquet openssh-server
``` bash 
apt-get install openssh-server
```
Démarrer le serveur SSH
``` bash 
systemctl start ssh
```
Modifier le fihier /etc/ssh/sshd_config
``` bash 
nano /etc/ssh/sshd_config
```
Remplacer 
``` bash 
PermitRootLogin without-password
```
par
``` bash 
PermitRootLogin yes
```
Enfin redemarrer le service ssh
``` bash 
service ssh restart
```
## Script de supervision des logs Apache (exécuté toutes les heures)

Installation du script dans le répertoire **/opt/scripts** avec la commande:
``` bash
wget -P /opt/scripts https://gitlab.com/cesi-projects/cesi-system-project/raw/master/http_log.sh
```

## Script de régénaration des pages html (exécuté toutes les 5 minutes)

Installation du script dans le répertoire **/opt/scripts** avec la commande:
``` bash
wget -P /opt/scripts https://gitlab.com/cesi-projects/cesi-system-project/raw/master/csv_to_html.sh
```
Afin que le script **csv_to_html.sh** fonctionne il faut installer les paquets **curl git python-setuptools**:
``` bash
apt-get install curl python-setuptools git
```
Puis récupérer l'outil de conversion csv2html depuis github:
``` bash
cd /opt/
git clone https://github.com/dbohdan/csv2html.git
python setup.py install
```
## Script de backup

Installation du paquet rdiff-backup
``` bash 
apt-get install rdiff-backup
```
Installation du paquet msmtp
``` bash 
apt-get install msmtp
```
Créer le parametrage de msmtp à la racine
``` bash 
nano ~/.msmtprc
```
Et y ajouter
``` bash 
account default
tls on
host smtp.gmail.com
port 587
from alouarrani@gmail.com
auth on
user alouarrani
password xxxxxxxxxxxxxx
tls_starttls on
tls_certcheck on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
```
Ici le paramètrage du msmtp est de sorte à utiliser l'adresse mail alouarrani@gmail.com ainsi que son mot de passe application (caché) pour envoyer les mails avec les serveurs gmail.

Installation du script dans le répertoire **/opt/scripts** avec la commande:
``` bash
wget -P /opt/scripts https://gitlab.com/cesi-projects/cesi-system-project/raw/master/diff_saves.sh
```

## Configurer l'automatisation des scripts avec crontab

Editer le fichier avec:
``` bash
crontab -e
```
Et ajouter les lignes:
``` bash
0 * * * * bash /opt/scripts/http_log.sh &> /dev/null
*/5 * * * * bash /opt/scripts/csv_to_html.sh &> /dev/null
0 0 * * * bash /opt/scripts/diff_saves.sh &> /dev/null
```


# Installation du Serveur DHCP

Pour réaliser le servseur DHCP il faut au préalable installer tous les fichiers nécessaires à l'aide de la commande:
``` bash
apt install isc-dhcp-server
apt install net-tools
```
Il faut passer en mode root et réaliser la commande 
``` bash
service network-manager stop
```
On édite ensuite le fichier dhcpd.conf qui se trouve dans le chemin suivant:
``` bash
nano /etc/dhcp/dhcpd.conf
```
On supprime ce qu'il contient et on le remplace par notre configuration qui est la suivante:
``` bash
option domain-name-servers 192.168.10.5, 192.168.10.6;  
option domain-name "carnofluxe.domain"; 
default-lease-time 1000;    
max-lease-time 2000;    
option routers 192.168.10.1 
subnet 192.168.10.0 netmask 255.255.255.0 { 
    range 192.168.10.100 192.168.10.200;    
    option subnet-mask 255.255.255.0;   
    option broadcast-address 192.168.10.255;    
}   
```
On peut ensuite utiliser la commande suivante afin de vérifier si la configuration ne contient pas d'erreur:
``` bash
dhcpd -t -cf /etc/dhcp/dhcpd.conf
```
Ensuite on modifie le fichier isc-dhcp-server qui se trouve dans le chemin suivant:
``` bash
/etc/default/isc-dhcp-server
```
et à la mention INTERFACESv4 "" on rentre entre les guillemets le nom de notre interfaces réseau
en l'occurence enp0s3 dans mon cas
Puis on modifie le fichier interfaces qui à le chemin suivant:
``` bash
nano /etc/network/interfaces
```
et on ajoute les mentions suivantes:
iface enp0s3 inet static
address 192.168.10.5
netmask 255.255.255.0
dns-nameservers 192.168.10.5 192.10.6
gateway 192.168.10.1

On redemarre notre Debian à l'aide de la commande :
``` bash
reboot
```
Enfin on démarre notre serveur DHCP :
``` bash
systemctl start isc-dhcp-server
```
Et on vérifie son fonctionnement à l'aide de la commande:
``` bash
systemctl status isc-dhcp-server
```


# Installation du Serveur DNS

On a réalisé notre serveur DNS sur VirtualBox, il faut donc créer un Reseau NAT sur notre VirtualBox pour pourvoir
faire communiquer deux machines virtuels.
J'ai également directement mis les lignes necessaire dans chacun des fichiers du serveur DNS maitre pour avoir
un serveur DNS esclave.

Pour réaliser le servseur DNS il faut au préalable installer tous les fichiers nécessaires à l'aide de la commande:
``` bash
apt install net-tools
apt install bind9
apt install dnsutils
apt install resolvconf
```
La première étape est de donner une IP fixe au serveur commme réaliser pour le servveur dhcp en modifiant le ficher interfaces:
``` bash
nano /etc/network/interfaces
```
Et de le modifier comme pour le serveur DHCP pour fixer son IP
Ensuite il faut modifier le fichier hostname suivant:
``` bash
nano /etc/hostname 
```
Et d'y rentrer le FQDN qui dans notre cas est :
ns1.carnofluxe.domain

Il faut modifier ensuite le fichier hosts qui à le chemin d'accès suivant:
``` bash
nano /etc/hosts
```
On modifie de la manière suivante :
``` bash
127.0.0.1       localhost
127.0.1.1       ns1.carnofluxe.domain
192.168.10.5    ns1.carnofluxe.domain
(192.168.10.6    slave.carnofluxe.domain) Pour le DNS esclave
```
Ensuite il faut modifier le fichier resolv.conf qui à le chemin suivant:
``` bash
nano /etc/resolv.conf
```
On ecrit à l'intérieur les mentions suivantes:  
domain carnofluxe.domain       
search carnofluxe.domain    
nameserver 192.168.10.5 (IP fixe de notre serveur DNS)  
(nameserver 192.168.10.6 qui est l'IP du serveur DNS esclave)   

On peut également modifier le fichier base qui à le chemin d'accès suivant:
``` bash
/etc/resolvconf/resolv.conf.d/base
```
On écrit la même chose que dans le fichier resolv.conf, cette manipulation permet
de garder notre configuration dans le resolv même apres un reboot.
Après cela on utilise la commande:
``` bash
service resolvconf restart
```


Il est ensuite necessaire de modifier le fichier named.conf.local qui à le chemin d'accès suivant:
``` bash
nano /etc/bind/named.conf.local
```
Où on ajoute les mentions suivantes:    
zone "carnofluxe.domain" {  
        type master;    
        file "/etc/bind/db.carnofluxe.domain";  
        allow-transfer {192.168.10.6;}; (Necessaire pour le DNS esclave)    
 }; 
 zone "10.168.192.in-addr.arpa" {   
        type master;    
        file "/etc/bind/db.10.168.192.in-addr.arpa";    
        allow-transfer {192.168.10.6;}; (Necessaire pour le DNS esclave)    
 }; 
 
Il faut donc modifier les fichiers que l'on a annoncé ci-desssus:
    Pour le premier:
``` bash
nano /etc/bind/db.carnofluxe.domain
```
On y marque les mentions suivantes: 
$TTL 10800  
$ORIGIN carnofluxe.domain.  
@       IN SOA ns1.carnofluxe.domain. root.carnofluxe. (    
        20160505;   
        3h; 
        1h: 
        1w; 
        1h);    
@       IN NS   ns1.carnofluxe.domain.  
ns1     IN A    192.168.10.5    
(slave   IN A    192.168.10.6) Pour le serveur DNS esclave  
www     IN A    192.168.10.10   
wiki    IN A    192.168.10.10

On réalise une modification similaire dans l'autre fichier:
``` bash
nano /etc/bind/db.10.168.192.in-addr.ap
```

$TTL 10800  
$ORIGIN 10.168.192.in-addr.arpa.    
@       IN SOA  ns1.carnofluxe.domain. root.carnofluxe.domain ( 
        2015090702              ; Serial    
            604800              ; Refresh   
             86400              ; Retry 
           2419200              ; Expire    
            604800              ; Negative Cache TTL    
);  
@       IN      NS      slave.carnofluxe.domain.    
@       IN      NS      ns1.carnofluxe.domain.  
5       IN      PTR     ns1.carnofluxe.domain.  
6       IN      PTR     ns2.carnofluxe.domain.  
10      IN      PTR     www.carnofluxe.domain.  
10      IN      PTR     wiki.carnofluxe.domain.

On peut vérifier notre configuration à l'aide de la commande 
``` bash
named-checkconf -z
```
Pour finir on restart le service DNS:
``` bash
service bind9 restart
```
On vérifie le bon focntionnemenet du service DNS avec la commande :
``` bash
dig ns1.carnofluxe.domain
et
dig -x 192.168.10.5
```


# Installation du Serveur DNS esclave

Sur une autre machine virtuel il faut réinstaller les services suivants:
``` bash
apt install net-tools bind9 dnsutils curl
```
Dans un premier temps il faut configurer, comme le serveur DNS maitre les fichiers suivant:

/etc/network/interfaces => Pour donner une IP fixe au serveur.  
/etc/hostname => Pour changer le FQDN du serveur.   
/etc/hosts => Pour la résolution locale, même manière que le serveur maitre.     
/etc/resolv.conf => Indiquer les serveurs DNS qui devront être utilisés.        


Et on modifie le fichier named.conf.local avec le chemin suivant:
``` bash
nano /etc/bind/named.conf.local
```
zone "carnofluxe.domain" {  
    type slave; 
    masters { 192.168.10.5; };  
    file "var/lib/bind/db.carnofluxe.domain";   
};  
zone "10.168.192.in-addr.arpa" {    
    type slave; 
    masters { 192.168.10.5; };  
    file "/var/lib/blind/db.10.168.192.in-addr.arpa";   
};  
Ensuite on fait la commande suivante:
``` bash
service bind9 restart
```

## Installation et configuration du client ssh
Installation du paquet openssh-client
``` bash 
apt-get install openssh-client
```
Créer une clé SSH en laissant le chemin par défaut (~/.ssh/id_rsa) et en ne mettant pas de passphrase.
``` bash 
ssh-keygen
```
Copier la clé du client sur le serveur
``` bash
ssh-copy-id -i /root/.ssh/id_rsa.pub root@192.168.10.10
```

## Installer le script de supervision
``` bash
mkdir /opt/scripts
cd /opt/scripts/
wget -P /opt/scripts https://gitlab.com/cesi-projects/cesi-system-project/raw/master/supervision.sh
```
## Configurer l'automatisation du avec crontab

Editer le fichier avec:
``` bash
crontab -e
```
Et ajouter les lignes:
``` bash
*/5 * * * * bash /opt/scripts/supervision.sh &> /dev/null
```


